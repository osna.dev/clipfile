package main

import (
    "fmt"
    "flag"
    "os"
    "io/ioutil"
    "github.com/atotto/clipboard"
)
var (
    verbose bool
)

func init() {
    flag.BoolVar(&verbose, "v", false, "enable verbose flag")
}

func main() {
    flag.Usage = func() {
        fmt.Fprintf(flag.CommandLine.Output(), "Usage of clipfile:\n")
        fmt.Fprintf(flag.CommandLine.Output(), "  clipfile <path>\n\n")
        fmt.Fprintf(flag.CommandLine.Output(), "  path    path to paste it's content in clipboard.\n")
        flag.PrintDefaults()
    }
    flag.Parse()

    if flag.NArg() == 0 {
        flag.Usage()
        os.Exit(1)
    }

    path := flag.Arg(0)
    var (
        out []byte
        err error
    )
    if path == "-" {
        out, err = ioutil.ReadAll(os.Stdin)
    } else {
        out, err = ioutil.ReadFile(path)
    }
    if err != nil {
        fmt.Fprintf(os.Stderr, "%s\n", err)
        os.Exit(1)
    }
    if err := clipboard.WriteAll(string(out)); err != nil {
        fmt.Fprintf(os.Stderr, "%s\n", err)
        os.Exit(1)
    }

    os.Exit(0)
}
